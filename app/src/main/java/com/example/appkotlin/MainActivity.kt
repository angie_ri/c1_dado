package com.example.appkotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import kotlin.random.Random

class MainActivity : AppCompatActivity() {

    lateinit var diceImagen : ImageView //instacia la variable

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val buttonRoll: Button = findViewById(R.id.buttonRoll)
        buttonRoll.text ="Botón ROll"  // cambia el texto del boton
        buttonRoll.setOnClickListener{

            Toast.makeText(this,"Nuevo valor",Toast.LENGTH_SHORT).show()//hace que muestre un msj
            diceImagen = findViewById(R.id.dice_image)  //inicializa
            rollDice();

        }
    }

    private fun rollDice(){
        val resulText : TextView= findViewById(R.id.resultado)
        //resulText.text= "Conseguir Resultado"
        resulText.text = "Dado es:"
        val randomInt = Random.nextInt(6) +1

        val imageRandom = when (randomInt) {
            1 -> R.drawable.dice_1
            2 -> R.drawable.dice_2
            3 -> R.drawable.dice_3
            4 -> R.drawable.dice_4
            5 -> R.drawable.dice_5
            else -> R.drawable.dice_6
        }
        diceImagen.setImageResource(imageRandom)
    }
    private fun ejemplos()
    {
        var biEn = "HOLA Mundo KOtlin"
        println(biEn)
    }
}
